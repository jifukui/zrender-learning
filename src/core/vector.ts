/**
 * @deprecated
 * Use zrender.Point class instead
 */
import { MatrixArray } from './matrix';

/* global Float32Array */

// const ArrayCtor = typeof Float32Array === 'undefined'
//     ? Array
//     : Float32Array;

export type VectorArray = number[]
/**
 * 创建一个向量对象
 * @param x x坐标的值
 * @param y y坐标的值
 * @returns
 */
export function create(x?: number, y?: number): VectorArray {
    if (x == null) {
        x = 0;
    }
    if (y == null) {
        y = 0;
    }
    return [x, y];
}

/**
 * 向量值的拷贝
 * @param out 目的向量
 * @param v 源向量
 * @returns
 */
export function copy<T extends VectorArray>(out: T, v: VectorArray): T {
    out[0] = v[0];
    out[1] = v[1];
    return out;
}

/**
 * 拷贝一个向量
 * @param v 向量源
 * @returns
 */
export function clone(v: VectorArray): VectorArray {
    return [v[0], v[1]];
}

/**
 * 设置向量的值
 * @param out 向量
 * @param a x坐标的值
 * @param b y坐标的值
 * @returns
 */
export function set<T extends VectorArray>(out: T, a: number, b: number): T {
    out[0] = a;
    out[1] = b;
    return out;
}

/**
 * 向量的加运算
 * @param out 向量
 * @param v1 向量1
 * @param v2 向量2
 * @returns
 */
export function add<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray): T {
    out[0] = v1[0] + v2[0];
    out[1] = v1[1] + v2[1];
    return out;
}

/**
 *
 * @param out 向量缩放后相加
 * @param v1 向量1
 * @param v2 向量2 被缩放对象
 * @param a 缩放值
 * @returns
 */
export function scaleAndAdd<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray, a: number): T {
    out[0] = v1[0] + v2[0] * a;
    out[1] = v1[1] + v2[1] * a;
    return out;
}

/**
 * 向量的减运算
 * @param out 输出的向量的值
 * @param v1
 * @param v2
 * @returns
 */
export function sub<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray): T {
    out[0] = v1[0] - v2[0];
    out[1] = v1[1] - v2[1];
    return out;
}

/**
 * 计算向量的长度
 * @param v 向量
 * @returns
 */
export function len(v: VectorArray): number {
    return Math.sqrt(lenSquare(v));
}
export const length = len;

/**
 * 向量长度的平方
 * @param v 向量
 * @returns
 */
export function lenSquare(v: VectorArray): number {
    return v[0] * v[0] + v[1] * v[1];
}
export const lengthSquare = lenSquare;

/**
 * 向量的乘法
 * @param out
 * @param v1
 * @param v2
 * @returns
 */
export function mul<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray): T {
    out[0] = v1[0] * v2[0];
    out[1] = v1[1] * v2[1];
    return out;
}

/**
 * 向量的除法
 * @param out
 * @param v1
 * @param v2
 * @returns
 */
export function div<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray): T {
    out[0] = v1[0] / v2[0];
    out[1] = v1[1] / v2[1];
    return out;
}

/**
 * 向量的点乘计算
 * @param v1
 * @param v2
 * @returns
 */
export function dot(v1: VectorArray, v2: VectorArray) {
    return v1[0] * v2[0] + v1[1] * v2[1];
}

/**
 * 向量的缩放计算
 * @param out
 * @param v
 * @param s
 * @returns
 */
export function scale<T extends VectorArray>(out: T, v: VectorArray, s: number): T {
    out[0] = v[0] * s;
    out[1] = v[1] * s;
    return out;
}

/**
 * 向量的归一化计算
 * @param out
 * @param v
 * @returns
 */
export function normalize<T extends VectorArray>(out: T, v: VectorArray): T {
    const d = len(v);
    if (d === 0) {
        out[0] = 0;
        out[1] = 0;
    }
    else {
        out[0] = v[0] / d;
        out[1] = v[1] / d;
    }
    return out;
}

/**
 * 计算向量的距离
 * @param v1
 * @param v2
 * @returns
 */
export function distance(v1: VectorArray, v2: VectorArray): number {
    return Math.sqrt(
        (v1[0] - v2[0]) * (v1[0] - v2[0])
        + (v1[1] - v2[1]) * (v1[1] - v2[1])
    );
}
export const dist = distance;

/**
 * 计算向量距离的平方
 * @param v1
 * @param v2
 * @returns
 */
export function distanceSquare(v1: VectorArray, v2: VectorArray): number {
    return (v1[0] - v2[0]) * (v1[0] - v2[0])
        + (v1[1] - v2[1]) * (v1[1] - v2[1]);
}
export const distSquare = distanceSquare;

/**
 * 计算负向量
 * @param out
 * @param v
 * @returns
 */
export function negate<T extends VectorArray>(out: T, v: VectorArray): T {
    out[0] = -v[0];
    out[1] = -v[1];
    return out;
}

/**
 * 向量间插值
 * @param out
 * @param v1
 * @param v2
 * @param t
 * @returns
 */
export function lerp<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray, t: number): T {
    out[0] = v1[0] + t * (v2[0] - v1[0]);
    out[1] = v1[1] + t * (v2[1] - v1[1]);
    return out;
}

/**
 * 矩阵左乘向量
 */
export function applyTransform<T extends VectorArray>(out: T, v: VectorArray, m: MatrixArray): T {
    const x = v[0];
    const y = v[1];
    out[0] = m[0] * x + m[2] * y + m[4];
    out[1] = m[1] * x + m[3] * y + m[5];
    return out;
}

/**
 * 求两个向量最小值
 */
export function min<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray): T {
    out[0] = Math.min(v1[0], v2[0]);
    out[1] = Math.min(v1[1], v2[1]);
    return out;
}

/**
 * 求两个向量最大值
 */
export function max<T extends VectorArray>(out: T, v1: VectorArray, v2: VectorArray): T {
    out[0] = Math.max(v1[0], v2[0]);
    out[1] = Math.max(v1[1], v2[1]);
    return out;
}
