import { Dictionary } from './types';

// Simple LRU cache use doubly linked list
// @module zrender/core/LRU
/**
 * 实例类型
 */
export class Entry<T> {
    //参数值
    value: T
    //键值
    key: string | number
    //下一个实例
    next: Entry<T>
    //上一个实例
    prev: Entry<T>
    /**
     * 构造函数
     * @param val
     */
    constructor(val: T) {
        this.value = val;
    }
}
/**
 * Simple double linked list. Compared with array, it has O(1) remove operation.
 * @constructor
 */

/**
 * 链表对象
 *
 */
export class LinkedList<T> {
    //实例的头部
    head: Entry<T>
    //实例的尾部
    tail: Entry<T>
    //长度
    private _len = 0

    /**
     * 插入
     * @param val
     * @returns
     */
    insert(val: T): Entry<T> {
        const entry = new Entry(val);
        this.insertEntry(entry);
        return entry;
    }

    /**
     * 插入实例到链表的尾部
     * @param entry
     */
    insertEntry(entry: Entry<T>) {
        if (!this.head) {
            this.head = this.tail = entry;
        }
        else {
            this.tail.next = entry;
            entry.prev = this.tail;
            entry.next = null;
            this.tail = entry;
        }
        this._len++;
    }

    /**
     * 删除实例
     * @param entry
     */
    remove(entry: Entry<T>) {
        const prev = entry.prev;
        const next = entry.next;
        if (prev) {
            prev.next = next;
        }
        else {
            // Is head
            this.head = next;
        }
        if (next) {
            next.prev = prev;
        }
        else {
            // Is tail
            this.tail = prev;
        }
        entry.next = entry.prev = null;
        this._len--;
    }

    /**
     * 获取实例的长度
     * @returns
     */
    len(): number {
        return this._len;
    }

    /**
     * 清空列表
     */
    clear() {
        this.head = this.tail = null;
        this._len = 0;
    }

}

/**
 * LRU缓存
 */
export default class LRU<T> {
    //链表
    private _list = new LinkedList<T>()
    //最大长度
    private _maxSize = 10
    //最
    private _lastRemovedEntry: Entry<T>
    //字典
    private _map: Dictionary<Entry<T>> = {}
    //构造函数
    constructor(maxSize: number) {
        this._maxSize = maxSize;
    }

    /**
     *
     * @param key 键值
     * @param value 参数
     * @returns
     */
    put(key: string | number, value: T): T {
        const list = this._list;
        const map = this._map;
        let removed = null;
        if (map[key] == null) {
            const len = list.len();
            // Reuse last removed entry
            let entry = this._lastRemovedEntry;

            if (len >= this._maxSize && len > 0) {
                // Remove the least recently used
                const leastUsedEntry = list.head;
                list.remove(leastUsedEntry);
                delete map[leastUsedEntry.key];

                removed = leastUsedEntry.value;
                this._lastRemovedEntry = leastUsedEntry;
            }

            if (entry) {
                entry.value = value;
            }
            else {
                entry = new Entry(value);
            }
            entry.key = key;
            list.insertEntry(entry);
            map[key] = entry;
        }

        return removed;
    }
    /**
     *
     * @param key
     * @returns
     */
    get(key: string | number): T {
        const entry = this._map[key];
        const list = this._list;
        if (entry != null) {
            // Put the latest used entry in the tail
            if (entry !== list.tail) {
                list.remove(entry);
                list.insertEntry(entry);
            }

            return entry.value;
        }
    }

    /**
     * 清空
     */
    clear() {
        this._list.clear();
        this._map = {};
    }
    /**
     * 长度
     * @returns
     */
    len() {
        return this._list.len();
    }
}