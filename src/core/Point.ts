import { MatrixArray } from './matrix';
/**
 * 定义坐标接口
 */
export interface PointLike {
    x: number
    y: number
}
/**
 * 定义坐标类
 */
export default class Point {
    //x坐标
    x: number
    //y坐标
    y: number
    /**
     * 构造函数
     * @param x 横坐标
     * @param y 纵坐标
     */
    constructor(x?: number, y?: number) {
        this.x = x || 0;
        this.y = y || 0;
    }

    /**
     * 坐标点的拷贝
     * @param other 传入被拷贝的坐标
     * @returns
     */
    copy(other: PointLike) {
        this.x = other.x;
        this.y = other.y;
        return this;
    }

    /**
     * 坐标点的拷贝
     * @returns
     */
    clone() {
        return new Point(this.x, this.y);
    }

    /**
     * 设置坐标点的值
     * @param x
     * @param y
     * @returns
     */
    set(x: number, y: number) {
        this.x = x;
        this.y = y;
        return this;
    }

    /**
     * 判断两个坐标点是否相等
     * @param other
     * @returns
     */
    equal(other: PointLike) {
        return other.x === this.x && other.y === this.y;
    }

    /**
     * 坐标点的加运算
     * @param other
     * @returns
     */
    add(other: PointLike) {
        this.x += other.x;
        this.y += other.y;
        return this;
    }
    /**
     * 坐标点的缩放运算
     * @param scalar
     */
    scale(scalar: number) {
        this.x *= scalar;
        this.y *= scalar;
    }
    /**
     * 坐标点的缩放相加操作
     * @param other
     * @param scalar
     */
    scaleAndAdd(other: PointLike, scalar: number) {
        this.x += other.x * scalar;
        this.y += other.y * scalar;
    }

    /**
     * 坐标点的减操作
     * @param other
     * @returns
     */
    sub(other: PointLike) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    /**
     * 计算两个点的值
     * @param other
     * @returns
     */
    dot(other: PointLike) {
        return this.x * other.x + this.y * other.y;
    }

    /**
     * 计算坐标点相对于原点的距离
     * @returns
     */
    len() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    /**
     * 计算坐标点长度的平方值
     * @returns
     */
    lenSquare() {
        return this.x * this.x + this.y * this.y;
    }

    /**
     * 坐标的归一化计算
     * @returns
     */
    normalize() {
        const len = this.len();
        this.x /= len;
        this.y /= len;
        return this;
    }

    /**
     * 计算连个点的距离
     * @param other
     * @returns
     */
    distance(other: PointLike) {
        const dx = this.x - other.x;
        const dy = this.y - other.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * 计算两个点的距离的平方值
     * @param other
     * @returns
     */
    distanceSquare(other: Point) {
        const dx = this.x - other.x;
        const dy = this.y - other.y;
        return dx * dx + dy * dy;
    }

    /**
     * 设置坐标点的值为0.0
     * @returns
     */
    negate() {
        this.x = -this.x;
        this.y = -this.y;
        return this;
    }

    /**
     * 矩阵转换为坐标点
     * @param m
     * @returns
     */
    transform(m: MatrixArray) {
        if (!m) {
            return;
        }
        const x = this.x;
        const y = this.y;
        this.x = m[0] * x + m[2] * y + m[4];
        this.y = m[1] * x + m[3] * y + m[5];
        return this;
    }
    /**
     * 坐标点转换为数组
     * @param out
     * @returns
     */
    toArray(out: number[]) {
        out[0] = this.x;
        out[1] = this.y;
        return out;
    }
    /**
     * 数组转换为坐标点
     * @param input
     */
    fromArray(input: number[]) {
        this.x = input[0];
        this.y = input[1];
    }
    /**
     * 设置传入坐标点的坐标值
     * @param p
     * @param x
     * @param y
     */
    static set(p: PointLike, x: number, y: number) {
        p.x = x;
        p.y = y;
    }
    /**
     * 将坐标p2的坐标复制到坐标p
     * @param p
     * @param p2
     */
    static copy(p: PointLike, p2: PointLike) {
        p.x = p2.x;
        p.y = p2.y;
    }
    /**
     * 获取坐标点的长度
     * @param p
     * @returns
     */
    static len(p: PointLike) {
        return Math.sqrt(p.x * p.x + p.y * p.y);
    }
    /**
     * 获取坐标点长度的平方
     * @param p
     * @returns
     */
    static lenSquare(p: PointLike) {
        return p.x * p.x + p.y * p.y;
    }
    /**
     *
     * @param p0
     * @param p1
     * @returns
     */
    static dot(p0: PointLike, p1: PointLike) {
        return p0.x * p1.x + p0.y * p1.y;
    }
    /**
     *
     * @param out
     * @param p0
     * @param p1
     */
    static add(out: PointLike, p0: PointLike, p1: PointLike) {
        out.x = p0.x + p1.x;
        out.y = p0.y + p1.y;
    }
    /**
     *
     * @param out
     * @param p0
     * @param p1
     */
    static sub(out: PointLike, p0: PointLike, p1: PointLike) {
        out.x = p0.x - p1.x;
        out.y = p0.y - p1.y;
    }
    /**
     *
     * @param out
     * @param p0
     * @param scalar
     */
    static scale(out: PointLike, p0: PointLike, scalar: number) {
        out.x = p0.x * scalar;
        out.y = p0.y * scalar;
    }
    /**
     *
     * @param out
     * @param p0
     * @param p1
     * @param scalar
     */
    static scaleAndAdd(out: PointLike, p0: PointLike, p1: PointLike, scalar: number) {
        out.x = p0.x + p1.x * scalar;
        out.y = p0.y + p1.y * scalar;
    }
    /**
     *
     * @param out
     * @param p0
     * @param p1
     * @param t
     */
    static lerp(out: PointLike, p0: PointLike, p1: PointLike, t: number) {
        const onet = 1 - t;
        out.x = onet * p0.x + t * p1.x;
        out.y = onet * p0.y + t * p1.y;
    }
}