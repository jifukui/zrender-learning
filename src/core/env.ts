declare const wx: {
    getSystemInfoSync: Function
};
/**
 * 浏览器类
 */
class Browser {
    firefox = false
    ie = false
    edge = false
    newEdge = false
    weChat = false
    version: string | number
}
/**
 * 环境
 */
class Env {
    browser = new Browser()
    node = false
    wxa = false
    worker = false

    canvasSupported = false
    svgSupported = false
    touchEventsSupported = false
    pointerEventsSupported = false
    domSupported = false
    transformSupported = false
    transform3dSupported = false
}

const env = new Env();
/**
 * 根据条件判断环境的类型和设置相关的支持情况
 */
if (typeof wx === 'object' && typeof wx.getSystemInfoSync === 'function') {
    env.wxa = true;
    env.canvasSupported = true;
    env.touchEventsSupported = true;
}
else if (typeof document === 'undefined' && typeof self !== 'undefined') {
    // In worker
    env.worker = true;
    env.canvasSupported = true;
}
else if (typeof navigator === 'undefined') {
    // In node
    env.node = true;
    env.canvasSupported = true;
    env.svgSupported = true;
}
else {
    detect(navigator.userAgent, env);
}

// Zepto.js
// (c) 2010-2013 Thomas Fuchs
// Zepto.js may be freely distributed under the MIT license.
/**
 * 检测
 * @param ua 用户代理
 * @param env 环境对象
 */
function detect(ua: string, env: Env) {
    const browser = env.browser;
    const firefox = ua.match(/Firefox\/([\d.]+)/);
    const ie = ua.match(/MSIE\s([\d.]+)/)
        // IE 11 Trident/7.0; rv:11.0
        || ua.match(/Trident\/.+?rv:(([\d.]+))/);
    const edge = ua.match(/Edge?\/([\d.]+)/); // IE 12 and 12+

    const weChat = (/micromessenger/i).test(ua);
    //火狐
    if (firefox) {
        browser.firefox = true;
        browser.version = firefox[1];
    }
    //ie
    if (ie) {
        browser.ie = true;
        browser.version = ie[1];
    }
    //edge
    if (edge) {
        browser.edge = true;
        browser.version = edge[1];
        browser.newEdge = +edge[1].split('.')[0] > 18;
    }

    // It is difficult to detect WeChat in Win Phone precisely, because ua can
    // not be set on win phone. So we do not consider Win Phone.
    //微信
    if (weChat) {
        browser.weChat = true;
    }
    //canvas的支持情况
    env.canvasSupported = !!document.createElement('canvas').getContext;
    //svg的支持情况
    env.svgSupported = typeof SVGRect !== 'undefined';
    //touch的支持情况
    env.touchEventsSupported = 'ontouchstart' in window && !browser.ie && !browser.edge;
    //点击的支持情况
    env.pointerEventsSupported = 'onpointerdown' in window
        && (browser.edge || (browser.ie && +browser.version >= 11));
    //dom的支持情况
    env.domSupported = typeof document !== 'undefined';
    //获取样式
    const style = document.documentElement.style;
    //3D的支持情况
    env.transform3dSupported = (
        // IE9 only supports transform 2D
        // transform 3D supported since IE10
        // we detect it by whether 'transition' is in style
        (browser.ie && 'transition' in style)
        // edge
        || browser.edge
        // webkit
        || (('WebKitCSSMatrix' in window) && ('m11' in new WebKitCSSMatrix()))
        // gecko-based browsers
        || 'MozPerspective' in style
    ) // Opera supports CSS transforms after version 12
      && !('OTransition' in style);

    // except IE 6-8 and very old firefox 2-3 & opera 10.1
    // other browsers all support `transform`
    //transform的支持情况
    env.transformSupported = env.transform3dSupported
        // transform 2D is supported in IE9
        || (browser.ie && +browser.version >= 9);

}


export default env;
